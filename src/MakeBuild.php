<?php

namespace Drupal\renderfilter;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;

class MakeBuild {

  /**
   * Convert children to render array.
   *
   * @param \DOMNode $node
   *   The dom node.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language.
   * @return array
   *   The render array.
   */
  public static function fromElementChildren($node, LanguageInterface $language) {
    $build = [];
    foreach ($node->childNodes as $childNode) {
      $build[] = static::fromElement($childNode, $language);
    }
    return $build;
  }

  /**
   * Convert the dom element to a render array.
   *
   * The render array is a tree of tag containers with some special elements.
   * Current special elements:
   * - link (for the dialogs module)
   * Other special elements can be added when the need arises.
   *
   * @param \DOMElement|\DOMText $node
   *   The dom node.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language.
   * @return array
   *   The render array.
   */
  public static function fromElement($node, LanguageInterface $language) {
    if ($node instanceof \DOMText) {
      $build['#markup'] = $node->wholeText;
    }
    elseif ($node instanceof \DOMElement) {
      if ($node->tagName === 'a' && $node->hasAttribute('href')) {
        try {
          $build = self::buildLink($node, $language);
        } catch (\InvalidArgumentException $e) {
          // URL was invalid.
          $build = self::buildTag($node, $language);
        }
      }
      else {
        $build = self::buildTag($node, $language);
      }
      $build['#renderfilter_tag'] = $node->tagName;
    }
    else {
      $build = [];
    }
    return $build;
  }

  /**
   * @param $node
   * @param \Drupal\Core\Language\LanguageInterface $language
   *
   * @return array
   */
  protected static function buildTag($node, LanguageInterface $language): array {
    $build['#type'] = 'html_tag';
    $build['#tag'] = $node->tagName;
    $attributes = static::buildAttributes($node, $language);
    if (isset($attributes['id'])) {
      $build['#id'] = $attributes['id'];
      unset($attributes['id']);
    }
    $build['#attributes'] = $attributes;
    $build += static::fromElementChildren($node, $language);
    return $build;
  }

  /**
   * @param $node
   * @param \Drupal\Core\Language\LanguageInterface $language
   *
   * @return mixed
   */
  protected static function buildLink($node, LanguageInterface $language) {
    $build['#type'] = 'link';

    if (
      $node->childNodes->length === 1
      && $node->childNodes->item(0) instanceof \DOMText
    ) {
      $build['#title'] = $node->nodeValue;
    }
    else {
      $build['#title'] = static::fromElementChildren($node, $language);
    }

    $attributes = static::buildAttributes($node, $language);

    if (isset($attributes['id'])) {
      $build['#id'] = $attributes['id'];
      unset($attributes['id']);
    }

    // Create url from href.
    $href = $attributes['href'];
    unset($attributes['href']);
    $baseUrl = Url::fromUri('base:/')->toString();
    if (substr($href, 0, strlen($baseUrl)) === $baseUrl) {
      $href = '/' . substr($href, strlen($baseUrl));
    }

    if (substr($href, 0, 1) === '/') {
      $url = Url::fromUserInput($href, ['language' => $language]);
    }
    else {
      $url = Url::fromUri($href, ['language' => $language]);
    }

    // We choose to set attributes in the url, not in render array.
    $url->setOption('attributes', $attributes);

    $build['#url'] = $url;
    return $build;
  }

  /**
   * Convert attributes to render array.
   *
   * @param \DOMElement $node
   *   The dom node.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language.
   * @return array
   *   The attributes render array.
   */
  protected static function buildAttributes($node, LanguageInterface $language) {
    $attributes = [];
    /** @var \DOMAttr $domAttribute */
    foreach ($node->attributes as $domAttribute) {
      $attributes[$domAttribute->name] = $domAttribute->value;
    }
    if (isset($attributes['class'])) {
      $attributes['class'] = explode(' ', $attributes['class']);
    }
    return $attributes;
  }

}
