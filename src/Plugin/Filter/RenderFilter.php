<?php

namespace Drupal\renderfilter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\renderfilter\MakeBuild;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter that parses ant then renders content.
 *
 * This helps with render magic like dialogs module.
 *
 * @Filter(
 *   id = "renderfilter",
 *   title = @Translation("Render filter"),
 *   description = @Translation("Parses and renders HTML, to apply render magic like dialogs module."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 * )
 */
class RenderFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * RenderFilter constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer, LanguageManagerInterface $languageManager) {
    $this->renderer = $renderer;
    $this->languageManager = $languageManager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('language_manager')
    );
  }

  /**
   * Performs the filter processing.
   *
   * @param string $text
   *   The text string to be filtered.
   * @param string|null $langcode
   *   The language code of the text to be filtered. Optional.
   *   Defaults to the content default language.
   *
   * @return \Drupal\filter\FilterProcessResult
   *   The filtered text, wrapped in a FilterProcessResult object, and possibly
   *   with associated assets, cacheability metadata and placeholders.
   *
   * @see \Drupal\filter\FilterProcessResult
   */
  public function process($text, $langcode) {
    // @see \Drupal\Core\Entity\EntityRepository::getTranslationFromContext
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
      $dependsOnContentLanguage = TRUE;
    }

    $language = $this->languageManager->getLanguage($langcode);
    $dom = Html::load($text);
    $bodyNode = $dom->getElementsByTagName('body')->item(0);
    $build = MakeBuild::fromElementChildren($bodyNode, $language);

    $renderContext = new RenderContext();
    $rendered = $this->renderer->executeInRenderContext($renderContext, function () use ($build) {
      return $this->renderer->render($build);
    });

    $result = new FilterProcessResult($rendered);
    $result = $result->merge(BubbleableMetadata::createFromRenderArray($build));
    if (!empty($dependsOnContentLanguage)) {
      $result->addCacheContexts(['languages:' . LanguageInterface::TYPE_CONTENT]);
    }
    return $result;
  }

}
